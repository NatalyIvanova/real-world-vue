export const filterLineProps = {
    'LINE_ID': 'lineId',
    'COLUMN_HEADER': 'columnHeader',
    'OPERATOR': 'operator',
    'COLUMN_VALUE': 'columnValue'
}

export const filterSelectConfig = {
    [filterLineProps['COLUMN_HEADER']]: {
        label: "Column",
        multiple: false,
        options: null
    },
    [filterLineProps['OPERATOR']]: {
        label: "Operator",
        multiple: false,
        options: null
    },
    [filterLineProps['COLUMN_VALUE']]: {
        label: "Value",
        multiple: true,
        options: null
    }
}

export const buildEmptyLine = ((currentObj, prop) => {
    currentObj[prop] = null;
    return currentObj;
});