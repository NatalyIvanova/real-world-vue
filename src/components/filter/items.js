export const operatorOptions = [
    'contains',
    'equals',
    'starts with',
    'ends with',
    'is empty',
    'is not empty',
    'is any of'
];

export const columnHeaderOptions = [
    'issuer',
    'trustee'
];

export const columnValueOptions = [
    'to be',
    'not to be'
];